import React, {useState, useEffect, Component} from 'react';

/* const Text = (props) => (
  const {name, handleName} =props;
  return (<>
    <p>Hola {name}</p>
    <button onClick={() => {
      handleName('José Miguel');
    }}>
      Cambiar
    </button>
  </>)
); */

class Text extends Component {
  render() {
    const {name, handleName} = this.props;
    return (
      <>
        <input type="text" name="nombre" value={name} onChange={(e) => {handleName(e.target.value)}} />
        <p>Hola {name}</p>
      </>
    );
  }
}

const Action = () => {
  const [name, handleName] = useState('Jaime');

  /* useEffect(() => {
    if (name !== 'Jaime') {
      alert('El componente se monto');
    }
  }, [name]); */

  return <Text name={name} handleName={handleName} />;
};

/* class Text extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      name: 'Jaime',
      age: 25,
    };
  }

  componentDidMount() {
    // this.setState({loading: false});
  }

  componentDidUpdate() {
    if (this.state.age > 25) {
      alert('Se ha cambiado el componente');
    }
  }

  componentWillUnmount() {

  }

  seeName = () => {
    console.log(this.state.name);
  };

  render() {
    this.seeName();
    const {name, age} = this.state;
    return (
      <>
        <p>Hola {name}</p>
        <p>Mi edad es {age}</p>
        <button onClick={() => {this.setState({name: 'José Miguel', age: 20})}}>
          Cambiar
        </button>
      </>
    );
  }
} */

export default Action;
