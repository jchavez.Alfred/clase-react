import React, {useState} from 'react';

const Noticia = ({noticia}) => (
  <div>
    <h4>{noticia.title}</h4>
    <span>{noticia.body}</span>
  </div>
);

const Arrays = () => {
  const [noticias] = useState([
    {
      title: 'Noticia 1',
      body: 'lorem ipsum'
    },
    {
      title: 'Noticia 2',
      body: 'lorem ipsum'
    },
  ]);
  return (
    <>
      {noticias.map((item) => {
        return <Noticia key={item.title} noticia={item} />;
      })}
    </>
  );
};

export default Arrays;